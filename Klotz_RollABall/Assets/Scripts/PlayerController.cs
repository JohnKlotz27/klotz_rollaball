﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;
    public Text timerText;
    float timer;
    public float maxSpeed;    //Maximum powerup speed 'To'
    public float incrementRate;    //The rate you want the speed to change
    public string powerUpName = "SpeedBoost";    //The tag of the powerup gameobject

    private Rigidbody rb;
    private int count;
    private Vector3 movement;
    private bool executePowerUp = false;  //Checks whether to execute the powerup 
    private float currentSpeed;    //The current speed of your powerup boost value

    void Start()
    {      
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";      
        timer = 0;
        
        currentSpeed = speed; //Set current speed to equal the minimum speed
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed); //As the game is running it will move the player in the direction the player wants to go

        timer += Time.deltaTime;    //The counting mechanic that changes the time on the timer in game
        timerText.text = timer.ToString("F"); //Converts float into a string so it can be printed on the UI
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag ("Pickup"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText ();

            transform.localScale += new Vector3(-.05F, -.05F, -.05F); //When the Player hits a "Pick Up" they will shrink

            speed = speed + incrementRate; //The player speeds up on collision with "Pick Up"


        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win";
        }
    }

    void Settimer()
    {
        SetCountText();
        while (winText.text != "You Win")
        {
            print(timer);
        }

    }
}